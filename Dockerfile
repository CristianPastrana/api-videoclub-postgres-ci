# se establece la imagen base 
FROM python:3.8

# se establece el working directory (directorio de trabajo) en el contenedor
WORKDIR /code
RUN cd /code

# copia el archivo de dependencias al working directory
COPY requirements.txt .
# copia el contenido del directorio local src y el server.py al working directory
RUN mkdir /app
COPY app/ /code/app
COPY server.py .
COPY config.py .
# se instalan dependencias
RUN pip3 install -r requirements.txt

# comando para ejecutar al inicio del contenedor
CMD [ "python", "server.py" ] 
