from flask import Flask
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy() 

def create_app(settings_module):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object('config.Config'+settings_module.capitalize())
    
    db.init_app(app)

    # Registro de los Blueprints
    from .rent import rent
    app.register_blueprint(rent)

    return app