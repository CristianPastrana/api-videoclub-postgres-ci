from app import db

#La clase Renta hereda de db.Model, lo que permite la equivalencia entre
#la tabla 'RENTA' de la base de datos y la clase Renta
class Renta(db.Model):
    __tablename__ = 'RENTA' #nombre que le daremos a nuestra base de datos
    object_id = db.Column(db.String(length=36), default=lambda: str(uuid.uuid4()), primary_key=True)
    client_id = db.Column(db.String(length=36), default=lambda: str(uuid.uuid4()))
    status = db.Column(db.String(30))
    until = db.Column(db.Date)

