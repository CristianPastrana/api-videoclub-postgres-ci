from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))

class Config(object):
    DEBUG = False
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"

class ConfigProduction(Config):
    ENV = 'production'
    SQLALCHEMY_DATABASE_URI = environ.get('DATABASE_URI')
    
class ConfigDevelopment(Config):
    DEBUG = True
    ENV = 'development'
    SQLALCHEMY_DATABASE_URI = environ.get('DATABASE_URI')

class ConfigTesting(Config):
    DEBUG = True
    ENV = 'testing'
    SQLALCHEMY_DATABASE_URI = environ.get('DATABASE_URI_TEST')
    TESTING = True
    WTF_CSRF_ENABLED = False
