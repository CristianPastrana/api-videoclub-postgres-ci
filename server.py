from app import create_app, db
from app.rent import routes, models
from app.rent.validations import fecha, status, validarUUID
from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))

settings_module = environ.get('APP_SETTINGS_MODULE')
print(settings_module)
print(settings_module.capitalize())
app = create_app(settings_module)

if __name__ == "__main__":
    with app.app_context():
        db.create_all()
    app.run(port=4000, host="0.0.0.0") #ejecucion del servidor con puerto y host definidos

