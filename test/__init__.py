import unittest

from app import create_app, db
from app.rent.models import Renta


class BaseTestClass(unittest.TestCase):

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app(settings_module="Testing")
        self.client = self.app.test_client()
        self.url_rent = "/rentas/"

        # Crea un contexto de aplicación
        with self.app.app_context():
            # Crea las tablas de la base de datos
            db.create_all()

            rent_1 = Renta()
            rent_1.object_id = "ca72a2ab-0000-0000-0000-68a041752017"
            rent_1.client_id = "b172a2ab-0000-0000-0000-688241072017"
            rent_1.status = "DELIVERY_TO_RENT"
            rent_1.until = "2020/10/01"
                            
            db.session.add(rent_1)
            db.session.commit()

    def tearDown(self):
        """teardown all initialized variables."""
        with self.app.app_context():
            # Elimina todas las tablas de la base de datos
            db.session.remove()
            db.drop_all()


