import unittest

from app.rent.models import Renta
from . import BaseTestClass


class BlogClientTestCase(BaseTestClass):

    def test_get_all_rents(self):
        response = self.client.get(self.url_rent)
        self.assertEqual(200, response.status_code)

    def test_add_new_rent(self):
        xml_renta = "<rent><object_id>b172a4ab-1111-4532-bd68-68a041752017</object_id><client_id>b172a4ab-6666-4532-bd68-688241072017</client_id>\
            <details><status>RETURN</status><until>2020/10/01</until></details></rent>"
        response = self.client.put(self.url_rent, data=xml_renta)
        self.assertEqual(201, response.status_code)

    def test_add_empty_rent(self):
        xml_renta_vacio = ""
        response = self.client.put(self.url_rent, data=xml_renta_vacio)
        self.assertEqual(400, response.status_code)

    #verifica si la peticion del "PUT" tuvo un error de validacion del formato del XML, es decir, si el codigo de estado es 400   
    def test_add_rent_with_invalid_xml_format(self):
        xml_renta_formato_xml_invalido = "<rent><obj3t_id>670b9562-b30d-52d5-b827-655787665500</object_id><client_id>4272a2ab-6666-4532-bd68-688241072017\
            </client_id><details><status>DELIVERY_TO_RENT</status><until>2020/10/15</until></details></rent>"
        response = self.client.put(self.url_rent, data=xml_renta_formato_xml_invalido)
        self.assertEqual(400, response.status_code)
    
    #verifica si la peticion del "PUT" tuvo un error de validacion con el objeto UUID, es decir, si el codigo de estado es 400   
    def test_add_rent_with_invalid_objectUUID_code(self):
        xml_renta_codigo_objectUUID_invalido = "<rent><object_id>a2ab-5900-4532-bd68-68a041752017</object_id><client_id>\
                4272a2ab-6666-4532-bd68-688241072017</client_id><details><status>DELIVERY_TO_RENT</status>\
                    <until>2020/10/15</until></details></rent>"
        response = self.client.put(self.url_rent, data=xml_renta_codigo_objectUUID_invalido)
        self.assertEqual(400, response.status_code)

    #verifica si la peticion del "PUT" tuvo un error de validacion con el cliente UUID, es decir, si el codigo de estado es 400   
    def test_add_rent_with_invalid_clientUUID_code(self):
        xml_renta_codigo_clientUUID_invalido = "<rent><object_id>670b9562-b30d-52d5-b827-655787665500</object_id><client_id>\
                4272a2ab-6-4532-bd68-688241072017</client_id><details><status>DELIVERY_TO_RENT</status>\
                    <until>2020/10/15</until></details></rent>"
        response = self.client.put(self.url_rent, data=xml_renta_codigo_clientUUID_invalido)
        self.assertEqual(400, response.status_code)

    #verifica si la peticion del "PUT" tuvo un error de validacion del estado, es decir, si el codigo de estado es 400   
    def test_add_rent_with_invalid_status(self):
        xml_renta_estado_invalido = "<rent><object_id>670b9562-b30d-52d5-b827-655787665500</object_id><client_id>\
                4272a2ab-6666-4532-bd68-688241072017</client_id><details><status>RENTED</status>\
                    <until>2020/10/15</until></details></rent>"
        response = self.client.put(self.url_rent, data=xml_renta_estado_invalido)
        self.assertEqual(400, response.status_code)

    #verifica si la peticion del "PUT" tuvo un error de validacion con la fecha, es decir, si el codigo de estado es 400   
    def test_add_rent_with_invalid_date(self):
        xml_renta_fecha_invalida = "<rent><object_id>670b9562-b30d-52d5-b827-655787665500</object_id><client_id>\
                4272a2ab-6666-4532-bd68-688241072017</client_id><details><status>DELIVERY_TO_RENT</status>\
                    <until>2020-10-15</until></details></rent>"
        response = self.client.put(self.url_rent, data=xml_renta_fecha_invalida)
        self.assertEqual(400, response.status_code)

    def test_delete_rent(self):
        codigo_object_uuid = "ca72a2ab-0000-0000-0000-68a041752017"
        response = self.client.delete(self.url_rent+codigo_object_uuid)
        self.assertEqual(204, response.status_code)     


if __name__ == '__main__':
    unittest.main()
